module.exports = {
  mode: 'jit',
  purge: [
    './public/**/*.html',
    './src/**/*.tsx',
  ],
  darkMode: true, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
