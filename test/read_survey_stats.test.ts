import 'reflect-metadata';
import Survey, { SurveyChoice } from "../src/domain/entities/survey";
import { RegisteredUser } from "../src/domain/entities/user";
import ReadSurveyStats from "../src/domain/use_cases/read_survey_stats";
import container from "../src/core/di";

const user1 = new RegisteredUser({id: 1, email: '', age: 20, country: 'France'});
const user2 = new RegisteredUser({id: 2, email: '', age: 30, country: 'Spain'});
const user3 = new RegisteredUser({id: 3, email: '', age: 40, country: 'Spain'});

const readSurveyStats: ReadSurveyStats = container.get(ReadSurveyStats);

let survey: Survey;

beforeEach(() => {
    survey = new Survey({
        id: 1,
        question: 'Test question',
        choices: [
            new SurveyChoice({id: 1, label: 'Yes', value: 'yes', answerers: [user1.anon(), user3.anon()]}),
            new SurveyChoice({id: 2, label: 'No', value: 'no', answerers: [user2.anon()]}),
        ]
    });
})

test('Given 1 answer NO and 2 answers YES, should have 2 YES and 1 NO', () => {
    readSurveyStats.call(survey);

    expect(survey.stats?.choiceStats[0].count).toBe(2);
    expect(survey.stats?.choiceStats[1].count).toBe(1);
});

test('Given 1 answer NO and 2 answers YES, should have 67% of YES and 33% of NO', () => {
    readSurveyStats.call(survey);

    expect(survey.stats?.choiceStats[0].getPercent()).toBe(67);
    expect(survey.stats?.choiceStats[1].getPercent()).toBe(33);
});

test('Given one french user answered YES and two spanish users answered both YES and NO, should have 1 Yes and 1 No for Spain and 1 Yes for France', () => {
    readSurveyStats.call(survey);

    expect(survey.stats?.choiceStats[0].countries[0]?.count).toBe(1);
    expect(survey.stats?.choiceStats[0].countries[1]?.count).toBe(1);
    expect(survey.stats?.choiceStats[1].countries[0]?.count).toBe(1);
    expect(survey.stats?.choiceStats[1].countries[1]?.count).toBe(undefined);
});

test('Given one french user answered YES and two spanish users answered both YES and NO, should have 50% of Yes and 50% of No for Spain and 100% of Yes for France', () => {
    readSurveyStats.call(survey);

    expect(survey.stats?.choiceStats[0].countries[0]?.getPercent()).toBe(50);
    expect(survey.stats?.choiceStats[0].countries[1]?.getPercent()).toBe(50);
    expect(survey.stats?.choiceStats[1].countries[0]?.getPercent()).toBe(100);
});
