import container from '../src/core/di';
import SurveyNotifier from '../src/domain/adapters/notifiers/survey_notifier';
import SurveyRepository from '../src/domain/adapters/repositories/survey_repository';
import Survey, { SurveyChoice } from '../src/domain/entities/survey';
import { RegisteredUser } from '../src/domain/entities/user';
import GetSurveyRecomendation from '../src/domain/use_cases/get_survey_recomendation';

class StubSurveyRepository extends SurveyRepository {
    surveys: Survey[];

    constructor(surveys: Survey[]) {
        super();
        this.surveys = surveys;
    }

    update(survey: Survey): void {
        throw new Error('Method not implemented.');
    }
    searchSurvey(searchTerm: string): Survey[] {
        throw new Error('Method not implemented.');
    }
    getSurveys(user: RegisteredUser): Survey[] {
        return this.surveys;
    }
}

class StubSurveyNotifier extends SurveyNotifier {
    sendRecomendation(recomendedSurvey: Survey[]): void {}
}

const userSelf = new RegisteredUser({id: 1, email: '', age: 20, country: 'France', surveysTaken: [1]});
const otherUser = new RegisteredUser({id: 2, email: '', age: 30, country: 'Spain', surveysTaken: [1, 2]});

let surveys: Survey[];
let getSurveyRecomendation: GetSurveyRecomendation;

let firstSurvey: Survey;
let lastSurvey: Survey;

beforeEach(() => {
    surveys = [
        new Survey({
            id: 1,
            question: 'Test question',
            choices: [
                new SurveyChoice({id: 1, label: 'Yes', value: 'yes', answerers: [userSelf.anon(), otherUser.anon()]}),
                new SurveyChoice({id: 2, label: 'No', value: 'no', answerers: []}),
            ]
        }),
        new Survey({
            id: 2,
            question: 'Test question 2',
            choices: [
                new SurveyChoice({id: 1, label: 'Yes', value: 'yes', answerers: []}),
                new SurveyChoice({id: 2, label: 'No', value: 'no', answerers: [otherUser.anon()]}),
            ]
        })
    ];

    container.bind<SurveyRepository>(SurveyRepository).to(StubSurveyRepository);
    container.bind<SurveyNotifier>(SurveyNotifier).to(StubSurveyNotifier);

    getSurveyRecomendation = new GetSurveyRecomendation();

    firstSurvey = surveys[0];
    lastSurvey = surveys[1];
})

test('Given 2 surveys, one that I took and the other one that I did not, get the last one recomended', () => {
    getSurveyRecomendation.call(userSelf);

    expect(userSelf.recomendedSurveys).not.toContain(firstSurvey);
});