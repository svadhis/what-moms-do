import 'reflect-metadata';
import React from 'react';
import ReactDOM from 'react-dom';
import './styles/output.css'
import App from './App';
import reportWebVitals from './reportWebVitals';
import BrowsePosts from './domain/use_cases/browse_posts';
import PostRepositoryImpl from './application/repositories/post_repository';
import PostPresenterImpl from './application/presenters/post_presenter';
import PostController from './application/controllers/post_controller';
import PostRemoteSourceImpl from './data/remote/post_remote_source';
import PostLocalSourceImpl from './data/local/post_local_source';
import NetworkAdapter from './application/network_adapter';
import PostPresenter from './domain/adapters/presenters/post_presenter';
import { Container } from 'inversify';
import PostRepository from './domain/adapters/repositories/post_repository';
import { Provider } from 'inversify-react';
import container from './core/di';

ReactDOM.render(
  <React.StrictMode>
    <Provider container={container}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();