import { faCheck, faClock, faPoll } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "@hookstate/core";
import { useInjection } from "inversify-react";
import { useEffect } from "react";
import { Link } from "react-router-dom";
import PostController from "../../application/controllers/post_controller";
import PostViewModel from "../../application/viewmodels/post_viewmodel";
import { postsState } from "../states/blog_states";

export default function PostsPage() {
    const posts = useState(postsState).get();

    const postController: PostController = useInjection(PostController);

    useEffect(() => {
        postController.useBrowsePosts();
    }, [])

    function getPostInfo(post: PostViewModel) {
        if (post.readed == false) {
            return (
                    <div>
                        <FontAwesomeIcon icon={faClock} />
                        &nbsp; { post.readingTime } mn
                    </div>
            );
        }

        const surveysNumber = post.surveys.length;
        const surveysTaken = post.surveys.filter(s => s.taken).length;

        if (surveysNumber > 0) {
            if (surveysTaken < surveysNumber) {
                return (
                    <div>
                        <FontAwesomeIcon icon={faPoll} />
                        &nbsp; { surveysTaken } / { surveysNumber }
                    </div>
                )
            }

            return (
                <div>
                    <FontAwesomeIcon icon={faCheck} />
                </div>
            )
        }

        return (
            <div></div>
        )
    }

    return (
        <div className="flex flex-col p-6 space-y-4 text-left">
            { posts.map(post => (
                <Link to={`/post/${post.id}`}>
                    <div className="border border-gray-400 rounded-md p-3 bg-blue-200 text-gray-800 shadow-md">
                        <h4>{ post.title }</h4>
                        <div className="flex justify-between mt-3 text-gray-600 text-sm">
                            <span>{ post.date }</span>
                            { getPostInfo(post) }
                        </div>
                    </div>
                </Link>
            )) }
        </div>
    );
}