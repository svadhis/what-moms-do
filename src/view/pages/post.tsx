import { faCheck, faClock, faPoll } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "@hookstate/core";
import { useInjection } from "inversify-react";
import { useEffect } from "react";
import { useParams } from "react-router";
import PostController from "../../application/controllers/post_controller";
import PostViewModel from "../../application/viewmodels/post_viewmodel";
import { postState, showSurveyState } from "../states/blog_states";
import parse from 'html-react-parser';
import { userState } from "../states/user_states";
import { surveyState } from "../states/survey_states";
import Survey from "../../domain/entities/survey";

export default function PostPage() {
    const post = useState(postState).get();
    const user = useState(userState).get();
    const showSurvey = useState(showSurveyState).get();
    const setShowSurvey = useState(showSurveyState).set;

    const survey = useState(surveyState).get();
    const setSurvey = useState(surveyState).set;

    let { postID } = useParams<{postID: string}>();

    const postController: PostController = useInjection(PostController);

    useEffect(() => {
        postController.useReadPost(parseInt(postID));
        setShowSurvey(null);
    }, [])

    useEffect(() => {
        if (survey !== null) {
            showSurveyStats(survey);
            setSurvey(null);
        }
    }, [survey])

    function showSurveyStats(survey: Survey) {
        console.log('showing survey stats');
    }

    return post !== null ? (
            <div>
                <div className="border-b border-gray-400 p-4 bg-blue-200 text-gray-800 shadow-md">
                    <h1>{ post.title }</h1>
                    <div className="mt-3 text-gray-600 text-sm flex justify-around">
                        <span>{ post.date }</span>
                        <div>
                            <FontAwesomeIcon icon={faClock} />
                            &nbsp; { post.readingTime } mn
                        </div>
                    </div>
                </div>
                <div className="cover">
                    <img className="h-52 w-full object-cover" src={ post.cover } alt="" />
                </div>
                <div className="post-intro p-10 text-lg montserrat text-left bg-blue-50">
                    { parse(post.introduction) }
                </div>
                <div className="post-content p-4 text-left">
                    { parse(post.content) }
                </div>
                <div className="post-surveys">
                    { post.surveys.map(survey => (
                        <div>
                            <div onClick={ _ => !survey.taken && setShowSurvey(survey.id) }>
                                { survey.question }
                            </div>
                            <div className={ showSurvey == survey.id ? '' : 'hidden' }>
                                <div onClick={ _ => setShowSurvey(null) } className="fixed top-0 bottom-0 left-0 right-0 bg-black bg-opacity-60 z-10">
                                </div>
                                <div className="fixed bottom-0 w-full bg-white border border-gray-800 py-6 px-4 z-20">
                                    <div>
                                        { user?.children[0].name }
                                    </div>
                                    <div>
                                        { survey.question }
                                    </div>
                                    { survey.choices.map(choice => (
                                        <div>
                                            { choice.label }
                                        </div>
                                    )) }
                                    <button onClick={ _ => postController.useTakeSurvey(post.surveys[0], user!, 0) }>Soumettre</button>
                                </div>
                            </div>
                        </div>
                    )) }
                </div>
                <div className="fixed left-1/2 bottom-20">
                    { post.surveys.find(survey => survey.taken)
                        ? <div className="poll-button relative -left-1/2 px-2 py-2 flex justify-center space-x-2 bg-blue-400 text-white rounded-md text-2xl shadow-md">
                                { post.surveys.map(survey => (
                                    <FontAwesomeIcon icon={faPoll} className={ survey.taken ? 'opacity-50' : '' } />
                                )) }
                            </div>
                        : <div></div>
                    }
                </div>
            </div>

    ) : <div></div>;
}