import { createState } from '@hookstate/core';
import Survey from '../../domain/entities/survey';

export const surveyState = createState<Survey | null>(null);