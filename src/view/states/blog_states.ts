import { createState } from '@hookstate/core';
import PostViewModel from '../../application/viewmodels/post_viewmodel';
import Post from '../../domain/entities/post';

export const postState = createState<PostViewModel | null>(null);

export const postsState = createState<PostViewModel[]>([]);
export const showSurveyState = createState<number | null>(null);