import { createState } from '@hookstate/core';
import Sex from '../../core/utils/sex';
import RegisteredChild from '../../domain/entities/child';
import { RegisteredUser } from '../../domain/entities/user';

export const userState = createState<RegisteredUser | null>(new RegisteredUser({
    id: 1,
    email: 'imeian@gmail.com',
    firstName: 'Nicolas',
    lastName: 'Miquel',
    age: 37,
    country: 'France',
    children: [
        new RegisteredChild({
            id: 1,
            name: 'Rafael',
            birthYear: 2019,
            sex: Sex.MALE
        })
    ]
})); // ! To implement