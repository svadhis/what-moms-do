import { faHome, faPoll, faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link, useLocation } from "react-router-dom";

export default function MainMenu() {

    const location = useLocation();

    return (
        <nav className="border-t border-gray-800 bg-blue-200 bottom-0 left-0 right-0">
            <ul className="flex justify-evenly">
              <li className={`flex-1 py-3 uppercase text-2xl ` + (location?.pathname === '/' || location?.pathname === '/posts' ? 'active-menu-item' : '')}>
                <Link to="/"><FontAwesomeIcon icon={faHome} /></Link>
              </li>
              <li className={`flex-1 py-3 uppercase text-2xl ` + (location?.pathname === '/surveys' ? 'active-menu-item' : '')}>
                <Link to="/surveys"><FontAwesomeIcon icon={faPoll} /></Link>
              </li>
              <li className={`flex-1 py-3 uppercase text-2xl ` + (location?.pathname === '/profile' ? 'active-menu-item' : '')}>
                <Link to="/profile"><FontAwesomeIcon icon={faUser} /></Link>
              </li>
            </ul>
        </nav>
    );
}