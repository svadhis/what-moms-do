import { injectable } from "inversify";
import Survey from "../../entities/survey";

@injectable()
export default abstract class SurveyNotifier {
    abstract sendRecomendation(recomendedSurvey: Survey[]): void;
}