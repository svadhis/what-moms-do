import { injectable } from "inversify";
import Post from "../../entities/post";

@injectable()
export default abstract class PostPresenter {
    displayPost(post: Post): void {}
    displayPosts(posts: Post[]): void {}
    displaySearchResult(posts: Post[]): void {}
}