import { injectable } from "inversify";
import Survey from "../../entities/survey";

@injectable()
export default abstract class SurveyPresenter {
    displaySurveyStats(survey: Survey): void {}
    displaySearchResult(surveys: Survey[]): void {}

}