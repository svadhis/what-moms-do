import { injectable } from "inversify";
import { RegisteredUser } from "../../entities/user";

@injectable()
export default abstract class UserPresenter {
    displayUserProfile(user: RegisteredUser): void {}
}