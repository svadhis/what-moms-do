import { injectable } from "inversify";
import Post from "../../entities/post";
import { RegisteredUser } from "../../entities/user";

@injectable()
export default abstract class PostRepository {
    abstract getPost(postID: number): Post;
    abstract getPosts(user?: RegisteredUser): Post[];
    abstract searchPost(searchTerm: string): Post[];
}