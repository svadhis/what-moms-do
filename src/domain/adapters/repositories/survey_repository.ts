import { injectable } from "inversify";
import Survey from "../../entities/survey";
import { RegisteredUser } from "../../entities/user";

@injectable()
export default abstract class SurveyRepository {
    abstract update(survey: Survey): void;
    abstract searchSurvey(searchTerm: string): Survey[]
    abstract getSurveys(user: RegisteredUser): Survey[];

}