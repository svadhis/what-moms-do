import { injectable } from "inversify";
import { RegisteredUser } from "../../entities/user";

@injectable()
export default abstract class UserRepository {
    update(user: RegisteredUser): void {}
    abstract signUp(email: string): RegisteredUser;
    abstract signIn(email: string): RegisteredUser;
    abstract signOut(user: RegisteredUser): void;
}