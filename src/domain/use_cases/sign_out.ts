import { inject, injectable } from "inversify";
import UserRepository from "../adapters/repositories/user_repository";
import { RegisteredUser } from "../entities/user";
import BrowsePosts from "./browse_posts";

@injectable()
export default class SignOut {
    @inject(UserRepository) private userRepository!: UserRepository;
    @inject(BrowsePosts) private browsePosts!: BrowsePosts;

    call({user}: SignOutData) {
        this.userRepository.signOut(user);

        this.browsePosts.call();
    }
}

interface SignOutData {
    user: RegisteredUser;
}