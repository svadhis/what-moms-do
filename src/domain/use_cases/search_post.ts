import { inject, injectable } from "inversify";
import PostPresenter from "../adapters/presenters/post_presenter";
import PostRepository from "../adapters/repositories/post_repository";

@injectable()
export default class SearchPost {
    @inject(PostRepository) private postRepository!: PostRepository;
    @inject(PostPresenter) private postPresenter!: PostPresenter;

    call(searchTerm: string) {
        const posts = this.postRepository.searchPost(searchTerm);

        this.postPresenter.displaySearchResult(posts);
    }
}