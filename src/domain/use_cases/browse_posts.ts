import { inject, injectable } from "inversify";
import PostPresenter from "../adapters/presenters/post_presenter";
import PostRepository from "../adapters/repositories/post_repository";
import { RegisteredUser } from "../entities/user";

@injectable()
export default class BrowsePosts {
    @inject(PostRepository) private postRepository!: PostRepository;
    @inject(PostPresenter) private postPresenter!: PostPresenter;

    call(user?: RegisteredUser) {
        const posts = this.postRepository.getPosts(user);

        this.postPresenter.displayPosts(posts);
    }
}