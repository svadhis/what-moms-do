import { inject, injectable } from "inversify";
import UserRepository from "../adapters/repositories/user_repository";
import { RegisteredUser } from "../entities/user";
import BrowsePosts from "./browse_posts";

@injectable()
export default class SignIn {
    @inject(UserRepository) private userRepository!: UserRepository;
    @inject(BrowsePosts) private browsePosts!: BrowsePosts;

    call({email}: SignInData) {
        const user: RegisteredUser = this.userRepository.signIn(email);

        this.browsePosts.call(user);
    }
}

interface SignInData {
    email: string;
}