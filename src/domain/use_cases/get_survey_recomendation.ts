import { inject, injectable } from "inversify";
import SurveyNotifier from "../adapters/notifiers/survey_notifier";
import SurveyPresenter from "../adapters/presenters/survey_presenter";
import SurveyRepository from "../adapters/repositories/survey_repository";
import Survey from "../entities/survey";
import { RegisteredUser } from "../entities/user";

@injectable()
export default class GetSurveyRecomendation {
    @inject(SurveyRepository) private surveyRepository!: SurveyRepository;
    @inject(SurveyNotifier) private surveyNotifier!: SurveyNotifier;

    call(user: RegisteredUser) {
        const surveys: Survey[] = this.surveyRepository.getSurveys(user);

        user.setRecomendedSurveys(surveys);

        this.surveyNotifier.sendRecomendation(user.recomendedSurveys);
    }
}