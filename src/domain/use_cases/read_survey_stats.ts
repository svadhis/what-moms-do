import { inject, injectable } from "inversify";
import SurveyPresenter from "../adapters/presenters/survey_presenter";
import Survey from "../entities/survey"

@injectable()
export default class ReadSurveyStats {
    @inject(SurveyPresenter) private surveyPresenter!: SurveyPresenter;

    call(survey: Survey) {
        survey.setStats();

        this.surveyPresenter.displaySurveyStats(survey);
    }
}