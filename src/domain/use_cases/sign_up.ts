import { inject, injectable } from "inversify";
import UserPresenter from "../adapters/presenters/user_presenter";
import UserRepository from "../adapters/repositories/user_repository";
import { RegisteredUser } from "../entities/user";

@injectable()
export default class SignUp {
    @inject(UserRepository) private userRepository!: UserRepository;
    @inject(UserPresenter) private userPresenter!: UserPresenter;

    call({email}: SignUpData) {
        const user: RegisteredUser = this.userRepository.signUp(email);

        this.userPresenter.displayUserProfile(user);
    }
}

interface SignUpData {
    email: string;
}