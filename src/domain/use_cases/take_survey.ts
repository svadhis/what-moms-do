import { inject, injectable } from "inversify";
import SurveyPresenter from "../adapters/presenters/survey_presenter";
import SurveyRepository from "../adapters/repositories/survey_repository";
import UserRepository from "../adapters/repositories/user_repository";
import Survey from "../entities/survey"
import { RegisteredUser } from "../entities/user";
import ReadSurveyStats from "./read_survey_stats";

@injectable()
export default class TakeSurvey {
    @inject(UserRepository) private userRepository!: UserRepository;
    @inject(SurveyRepository) private surveyRepository!: SurveyRepository;

    @inject(ReadSurveyStats) private readSurveyStats!: ReadSurveyStats;

    call({survey, user, choiceID}: TakeSurveyParameters) {
        user.addSurvey(survey.id);
        this.userRepository.update(user);

        survey.addAnswerer(user, choiceID);
        this.surveyRepository.update(survey);

        this.readSurveyStats.call(survey);
    }
}

interface TakeSurveyParameters {
    survey: Survey;
    user: RegisteredUser;
    choiceID: number;
}