import { inject, injectable } from "inversify";
import SurveyPresenter from "../adapters/presenters/survey_presenter";
import SurveyRepository from "../adapters/repositories/survey_repository";

@injectable()
export default class SearchSurvey {
    @inject(SurveyRepository) private surveyRepository!: SurveyRepository;
    @inject(SurveyPresenter) private surveyPresenter!: SurveyPresenter;

    call(searchTerm: string) {
        const surveys = this.surveyRepository.searchSurvey(searchTerm);

        this.surveyPresenter.displaySearchResult(surveys);
    }
}