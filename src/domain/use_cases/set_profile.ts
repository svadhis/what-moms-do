import { inject, injectable } from "inversify";
import UserPresenter from "../adapters/presenters/user_presenter";
import UserRepository from "../adapters/repositories/user_repository";
import { RegisteredUser } from "../entities/user";

@injectable()
export default class SetProfile {
    @inject(UserRepository) private userRepository!: UserRepository;
    @inject(UserPresenter) private userPresenter!: UserPresenter;

    call({user, firstName, lastName, age, country}: SetProfileParameters) {
        user.setProfile(firstName, lastName, age, country);
        this.userRepository.update(user);

        this.userPresenter.displayUserProfile(user);
    }
}

interface SetProfileParameters {
    user: RegisteredUser;
    firstName: string,
    lastName: string,
    age: number,
    country: string
}