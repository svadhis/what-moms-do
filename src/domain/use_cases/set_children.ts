import { inject, injectable } from "inversify";
import UserPresenter from "../adapters/presenters/user_presenter";
import UserRepository from "../adapters/repositories/user_repository";
import Child from "../entities/child";
import { RegisteredUser } from "../entities/user";

@injectable()
export default class SetChildren {
    @inject(UserRepository) private userRepository!: UserRepository;
    @inject(UserPresenter) private userPresenter!: UserPresenter;

    call({user, children}: SetChildrenParameters) {
        user.setChildren(children);
        this.userRepository.update(user);

        this.userPresenter.displayUserProfile(user);
    }
}

interface SetChildrenParameters {
    user: RegisteredUser;
    children: Child[];
}