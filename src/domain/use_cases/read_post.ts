import { inject, injectable } from "inversify";
import PostPresenter from "../adapters/presenters/post_presenter";
import PostRepository from "../adapters/repositories/post_repository";

@injectable()
export default class ReadPost {
    @inject(PostRepository) private postRepository!: PostRepository;
    @inject(PostPresenter) private postPresenter!: PostPresenter;

    call(postID: number) {
        const post = this.postRepository.getPost(postID);

        this.postPresenter.displayPost(post);
    }
}