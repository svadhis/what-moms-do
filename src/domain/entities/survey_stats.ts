import { SurveyChoice } from "./survey";

export default class SurveyStats {
    choices: SurveyChoice[];
    choiceStats: ChoiceStat[];

    constructor({choices}: SurveyStatsParameters) {
        this.choices = choices;
        this.choiceStats = [];
    }

    setResults() {
        const totalCount = this.getTotalCount();

        this.choices.forEach(choice => {
            const choiceCount = this.getChoiceCount(choice);
            const countries: CountryStat[] = [];

            const occurences = choice.answerers.map(c => c.country);
            occurences.forEach(occurence => {
                let country: CountryStat | undefined = countries.find(c => c.value == occurence);
                if (country) {
                    country.add1();
                }
                else {
                    countries.push(new CountryStat({value: occurence, count: 1, totalCount: choiceCount}));
                }
            });

            this.choiceStats.push(new ChoiceStat({value: choice.value, count: choice.answerers.length, totalCount: totalCount}, {countries: countries}));
        });
    }

    private getTotalCount() {
        return this.choices.reduce((t, c) => t + c.answerers.length, 0);
    }

    private getChoiceCount(choice: SurveyChoice) {
        return choice.answerers.length;
    }
}

abstract class Stat {
    count: number;
    totalCount: number;

    constructor(count: number, totalCount: number) {
        this.count = count;
        this.totalCount = totalCount;
    }

    getPercent(): number {
        return Math.round(this.count / this.totalCount * 100);
    }

    add1() {
        this.count++;
    }
}

class ChoiceStat extends Stat {
    value: string;
    countries: CountryStat[];

    constructor({value, count, totalCount}: StatParameters, {countries}: ChoiceStatParameters) {
        super(count, totalCount);
        this.value = value;
        this.countries = countries;
    }
}

class CountryStat extends Stat {
    value: string;

    constructor({value, count, totalCount}: StatParameters) {
        super(count, totalCount);
        this.value = value;
    }
}

interface SurveyStatsParameters {
    choices: SurveyChoice[];
}

interface StatParameters {
    value: string;
    count: number;
    totalCount: number;
}

interface ChoiceStatParameters {
    countries: CountryStat[];
}