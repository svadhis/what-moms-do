import Survey from "./survey";

export default class Post {
    id: number;
    title: string;
    introduction: string;
    content: string;
    cover: string;
    date: Date;
    readed: boolean;
    surveys: Survey[];

    constructor({id, title, introduction, content, cover, date, readed, surveys}: PostParameters) {
        this.id = id;
        this.title = title;
        this.introduction = introduction ?? '';
        this.content = content;
        this.cover = cover;
        this.date = date;
        this.readed = readed;
        this.surveys = surveys;
    }

}

interface PostParameters {
    id: number;
    title: string;
    introduction?: string;
    content: string;
    cover: string;
    date: Date;
    readed: boolean;
    surveys: Survey[];
}