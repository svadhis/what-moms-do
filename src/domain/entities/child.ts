import Sex from "../../core/utils/sex";

export abstract class Child {
    birthYear: number;
    sex: Sex;

    constructor(birthYear: number, sex: Sex) {
        this.birthYear = birthYear;
        this.sex = sex;
    }
}

export default class RegisteredChild extends Child {
    id: number;
    name: string;
    birthYear: number;
    sex: Sex;

    constructor({id, name, birthYear, sex}: ChildParameters) {
        super(birthYear, sex);
        this.id = id;
        this.name = name;
        this.birthYear = birthYear;
        this.sex = sex;
    }

}

export class AnonymousChild extends Child {
    constructor(child: RegisteredChild) {
        super(child.birthYear, child.sex);
    }
}

interface ChildParameters {
    id: number;
    name: string;
    birthYear: number;
    sex: Sex;
}