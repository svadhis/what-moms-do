import SurveyStats from "./survey_stats";
import { AnonymousUser, RegisteredUser } from "./user";

export default class Survey {
    id: number;
    question: string;
    taken: boolean;
    choices: SurveyChoice[];
    stats: SurveyStats | undefined;

    constructor({id, question, taken, choices}: SurveyParameters) {
        this.id = id;
        this.question = question;
        this.taken = taken ?? false;
        this.choices = choices;
    }

    setStats() {
        this.stats = new SurveyStats({choices: this.choices});
        this.stats.setResults();
    }

    addAnswerer(user: RegisteredUser, choiceID: number) {
        const anonymousUser = user.anon();
        this.choices.find(c => c.id = choiceID)?.answerers.push(anonymousUser);
    }

}

export class SurveyChoice {
    id: number;
    label: string;
    value: string;
    answerers: AnonymousUser[];

    constructor({id, label, value, answerers}: SurveyChoiceParameters) {
        this.id = id;
        this.label = label;
        this.value = value;
        this.answerers = answerers ?? [];
    }
}

interface SurveyParameters {
    id: number;
    question: string;
    taken: boolean;
    choices: SurveyChoice[];
}

interface SurveyChoiceParameters {
    id: number;
    label: string;
    value: string;
    answerers?: AnonymousUser[];
}