import RegisteredChild, { AnonymousChild, Child } from "./child";
import Survey from "./survey";

abstract class User {
    age: number;
    country: string;
    children: Child[];

    constructor(age: number, country: string, children: Child[]) {
        this.age = age;
        this.country = country;
        this.children = children;
    }

    setFields(age: number, country: string) {
        this.age = age;
        this.country = country;
    }

    setChildren(children: Child[]) {
        this.children = children;
    }
}

export class RegisteredUser extends User {
    id: number;
    email: string;
    surveysTaken: number[];
    firstName: string;
    lastName: string;
    children: RegisteredChild[];
    recomendedSurveys: Survey[];

    constructor({id, email, firstName, lastName, age, country, children, surveysTaken}: UserParameters) {
        super(age, country, children ?? []);
        this.id = id;
        this.email = email;
        this.firstName = firstName ?? '';
        this.lastName = lastName ?? '';
        this.children = children ?? [];
        this.surveysTaken = surveysTaken ?? [];

        this.recomendedSurveys = [];
    }

    anon(): AnonymousUser {
        return new AnonymousUser(this);
    }

    addSurvey(surveyID: number) {
        this.surveysTaken.push(surveyID);
    }

    setRecomendedSurveys(surveys: Survey[]) {
        const recomendedSurveys = this.findRecomendedSurvey(surveys);

        this.recomendedSurveys = recomendedSurveys;
    }

    setProfile(firstName: string, lastName: string, age: number, country: string) {
        this.firstName = firstName;
        this.lastName = lastName;
        super.setFields(age, country);
    }

    private findRecomendedSurvey(surveys: Survey[]): Survey[] {
        const filteredSurveys = surveys.filter(s => !this.surveysTaken.find(st => st == s.id));

        return filteredSurveys;
    }

}

export class AnonymousUser extends User {
    constructor(user: RegisteredUser) {
        super(user.age, user.country, user.children.map(c => new AnonymousChild(c)));
    }
}

class UserProfile {
    firstName: string;
    lastName: string;
    age: number;
    country: string;

    constructor(firstName: string, lastName: string, age: number, country: string) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.country = country;
    }
}

interface UserParameters {
    id: number;
    email: string;
    firstName?: string;
    lastName?: string,
    age: number;
    country: string;
    children?: RegisteredChild[];
    surveysTaken?: number[];
}