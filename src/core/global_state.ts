import { createState } from "@hookstate/core";
import Route from "./utils/route";

export const routeState = createState(Route.HOME);