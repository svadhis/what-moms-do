import { routeState } from "../global_state";
import Route from "./route";

export default function navigateTo(route: Route) {
    routeState.set(_ => route);
}