import { Container } from "inversify";
import PostController from "../application/controllers/post_controller";
import NetworkAdapter from "../application/network_adapter";
import PostPresenterImpl from "../application/presenters/post_presenter";
import SurveyPresenterImpl from "../application/presenters/survey_presenter";
import PostRepositoryImpl, { PostLocalSource, PostRemoteSource } from "../application/repositories/post_repository";
import SurveyRepositoryImpl, { SurveyRemoteSource } from "../application/repositories/survey_repository";
import UserRepositoryImpl, { UserRemoteSource } from "../application/repositories/user_repository";
import PostLocalSourceImpl from "../data/local/post_local_source";
import PostRemoteSourceImpl from "../data/remote/post_remote_source";
import SurveyRemoteSourceImpl from "../data/remote/survey_remote_source";
import UserRemoteSourceImpl from "../data/remote/user_remote_source";
import PostPresenter from "../domain/adapters/presenters/post_presenter";
import SurveyPresenter from "../domain/adapters/presenters/survey_presenter";
import PostRepository from "../domain/adapters/repositories/post_repository";
import SurveyRepository from "../domain/adapters/repositories/survey_repository";
import UserRepository from "../domain/adapters/repositories/user_repository";
import BrowsePosts from "../domain/use_cases/browse_posts";
import ReadPost from "../domain/use_cases/read_post";
import ReadSurveyStats from "../domain/use_cases/read_survey_stats";
import TakeSurvey from "../domain/use_cases/take_survey";

const container = new Container();

container.bind<NetworkAdapter>(NetworkAdapter).toSelf();

container.bind<PostLocalSource>(PostLocalSource).to(PostLocalSourceImpl);

container.bind<PostRemoteSource>(PostRemoteSource).to(PostRemoteSourceImpl);
container.bind<UserRemoteSource>(UserRemoteSource).to(UserRemoteSourceImpl);
container.bind<SurveyRemoteSource>(SurveyRemoteSource).to(SurveyRemoteSourceImpl);

container.bind<PostRepository>(PostRepository).to(PostRepositoryImpl);
container.bind<UserRepository>(UserRepository).to(UserRepositoryImpl);
container.bind<SurveyRepository>(SurveyRepository).to(SurveyRepositoryImpl);

container.bind<PostPresenter>(PostPresenter).to(PostPresenterImpl);
container.bind<SurveyPresenter>(SurveyPresenter).to(SurveyPresenterImpl);

container.bind<BrowsePosts>(BrowsePosts).toSelf();
container.bind<ReadPost>(ReadPost).toSelf();
container.bind<TakeSurvey>(TakeSurvey).toSelf();
container.bind<ReadSurveyStats>(ReadSurveyStats).toSelf();

container.bind<PostController>(PostController).toSelf();

export default container;