import './App.css';
import { useState } from '@hookstate/core';
import { postsState } from './view/states/blog_states';
import { routeState } from './core/global_state';
import { BrowserRouter as Router, Switch, Route, Link, useLocation } from "react-router-dom";
import { useInjection } from 'inversify-react';
import PostController from './application/controllers/post_controller';
import PostsPage from './view/pages/posts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHome, faPoll, faUser } from '@fortawesome/free-solid-svg-icons';
import PostPage from './view/pages/post';
import MainMenu from './view/components/organisms/main_menu';

function App() {
  return (
    <Router>
      <div className="App flex flex-col h-screen">
        <div className="overflow-y-scroll flex-grow">
          <Switch>
            <Route path="/post/:postID">
            <PostPage />
            </Route>
            <Route path="/profile">
            <div>USERS</div>
            </Route>
            <Route path="/">
            <PostsPage />
            </Route>
          </Switch>
        </div>
        <MainMenu />
      </div>
    </Router>
  );
}

export default App;
