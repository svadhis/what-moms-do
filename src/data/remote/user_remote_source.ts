import { injectable } from "inversify";
import { UserRemoteSource } from "../../application/repositories/user_repository";
import { RegisteredUser } from "../../domain/entities/user";

@injectable()
export default class UserRemoteSourceImpl implements UserRemoteSource {
    update(user: RegisteredUser): RegisteredUser {
        return user;
    }
}