import { injectable } from "inversify";
import { SurveyRemoteSource } from "../../application/repositories/survey_repository";
import survey from "../../domain/entities/survey";

@injectable()
export default class SurveyRemoteSourceImpl implements SurveyRemoteSource {
    update(survey: survey): survey {
        return survey;
    }
}