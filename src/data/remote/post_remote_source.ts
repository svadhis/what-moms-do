import { injectable } from "inversify";
import { PostRemoteSource } from "../../application/repositories/post_repository";
import Post from "../../domain/entities/post";
import Survey, { SurveyChoice } from "../../domain/entities/survey";
import { RegisteredUser } from "../../domain/entities/user";

@injectable()
export default class PostRemoteSourceImpl implements PostRemoteSource {
    getPost(postID: number): Post {
        return new Post({
            id: 1,
            title: 'Titre',
            introduction: '<p>Intro non ?</p>',
            content: '<p>Bla bla bla</p><p>Ok ?</p>',
            cover: 'http://www.slate.fr/sites/default/files/styles/1060x523/public/lukasz-szmigiel-jfcviyfycus-unsplash.jpg',
            date: new Date(),
            readed: true,
            surveys: [
                new Survey({
                    id: 1,
                    question: 'Question 1 ?',
                    taken: true,
                    choices: [
                        new SurveyChoice({
                            id: 1,
                            label: 'Réponse 1',
                            value: 'r1'
                        }),
                        new SurveyChoice({
                            id: 2,
                            label: 'Réponse 2',
                            value: 'r2'
                        }),
                    ]
                }),
                new Survey({
                    id: 2,
                    question: 'Question 2 ?',
                    taken: false,
                    choices: [
                        new SurveyChoice({
                            id: 1,
                            label: 'Réponse 1',
                            value: 'r1'
                        }),
                        new SurveyChoice({
                            id: 2,
                            label: 'Réponse 2',
                            value: 'r2'
                        }),
                        new SurveyChoice({
                            id: 3,
                            label: 'Réponse 3',
                            value: 'r3'
                        }),
                    ]
                }),
            ]
        });
    }

    getPosts(user?: RegisteredUser): Post[] {
        return [
            new Post({
                id: 1,
                title: 'Titre',
                content: 'Bla bla bla',
                cover: 'http://www.slate.fr/sites/default/files/styles/1060x523/public/lukasz-szmigiel-jfcviyfycus-unsplash.jpg',
                date: new Date(),
                readed: true,
                surveys: [
                    new Survey({
                        id: 1,
                        question: '',
                        taken: true,
                        choices: []
                    }),
                    new Survey({
                        id: 2,
                        question: '',
                        taken: false,
                        choices: []
                    }),
                ]
            }),
            new Post({
                id: 2,
                title: 'Titre 2',
                content: 'Bla bla bla * 2',
                cover: 'http://www.slate.fr/sites/default/files/styles/1060x523/public/lukasz-szmigiel-jfcviyfycus-unsplash.jpg',
                date: new Date(),
                readed: false,
                surveys: []
            }),
            new Post({
                id: 3,
                title: 'Titre 3',
                content: 'Bla bla bla',
                cover: 'http://www.slate.fr/sites/default/files/styles/1060x523/public/lukasz-szmigiel-jfcviyfycus-unsplash.jpg',
                date: new Date(),
                readed: true,
                surveys: [
                    new Survey({
                        id: 3,
                        question: '',
                        taken: true,
                        choices: []
                    }),
                    new Survey({
                        id: 4,
                        question: '',
                        taken: true,
                        choices: []
                    }),
                ]
            }),
        ];
    }
}