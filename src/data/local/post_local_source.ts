import { injectable } from "inversify";
import { PostLocalSource } from "../../application/repositories/post_repository";
import Post from "../../domain/entities/post";
import Survey from "../../domain/entities/survey";

@injectable()
export default class PostLocalSourceImpl implements PostLocalSource {
    getPost(postID: number): Post {
        return new Post({
            id: 1,
            title: 'Titre',
            content: 'Bla bla bla',
            cover: 'http://www.slate.fr/sites/default/files/styles/1060x523/public/lukasz-szmigiel-jfcviyfycus-unsplash.jpg',
            date: new Date(),
            readed: true,
            surveys: [
                new Survey({
                    id: 1,
                    question: '',
                    taken: true,
                    choices: []
                }),
                new Survey({
                    id: 2,
                    question: '',
                    taken: false,
                    choices: []
                }),
            ]
        });
    }

    getPosts(): Post[] {
        return [
            new Post({
                id: 1,
                title: 'Titre',
                content: 'Bla bla bla',
                cover: 'http://www.slate.fr/sites/default/files/styles/1060x523/public/lukasz-szmigiel-jfcviyfycus-unsplash.jpg',
                date: new Date(),
                readed: true,
                surveys: []
            }),
            new Post({
                id: 2,
                title: 'Titre 2',
                content: 'Bla bla bla * 2',
                cover: 'http://www.slate.fr/sites/default/files/styles/1060x523/public/lukasz-szmigiel-jfcviyfycus-unsplash.jpg',
                date: new Date(),
                readed: false,
                surveys: []
            })
        ];
    }
}