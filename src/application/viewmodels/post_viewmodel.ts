import Post from "../../domain/entities/post";
import Survey from "../../domain/entities/survey";

export default class PostViewModel {
    id: number;
    title: string;
    _introduction: string;
    _content: string;
    cover: string;
    date: string;
    readed: boolean;
    surveys: Survey[];

    constructor(post: Post) {
        this.id = post.id;
        this.title = post.title;
        this._introduction = post.introduction;
        this._content = post.content;
        this.cover = post.cover;
        this.date = this.getFormattedDate(post.date);
        this.readed = post.readed;
        this.surveys = post.surveys;
    }

    get introduction(): string {
        return this._introduction === '' ? this.extractIntroFromContent().introduction : this._introduction;
    }

    get content(): string {
        return this._introduction === '' ? this.extractIntroFromContent().content : this._content;
    }

    get readingTime() {
        return Math.ceil(this.content.split(' ').length / 50);
    }

    private getFormattedDate(date: Date) {
        return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;
    }

    private extractIntroFromContent() {
        const splitContent: string[] = this._content.split('<p>');

        return {
            introduction: `<p>${splitContent[1]}`,
            content: `<p>${splitContent.slice(2)}`,
        };
    }
}