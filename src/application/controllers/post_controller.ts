import { inject, injectable } from "inversify";
import Survey from "../../domain/entities/survey";
import { RegisteredUser } from "../../domain/entities/user";
import BrowsePosts from "../../domain/use_cases/browse_posts";
import ReadPost from "../../domain/use_cases/read_post";
import TakeSurvey from "../../domain/use_cases/take_survey";

@injectable()
export default class PostController {
    @inject(BrowsePosts) private browsePosts!: BrowsePosts;
    @inject(ReadPost) private readPost!: ReadPost;
    @inject(TakeSurvey) private takeSurvey!: TakeSurvey;

    useBrowsePosts(user?: RegisteredUser): void {
        this.browsePosts.call(user);
    }

    useReadPost(postID: number): void {
        this.readPost.call(postID);
    }

    useTakeSurvey(survey: Survey, user: RegisteredUser, choiceID: number): void {
        this.takeSurvey.call({
            survey: survey,
            user: user,
            choiceID: choiceID
        });
    }
}