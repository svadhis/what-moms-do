import { inject, injectable } from "inversify";
import PostRemoteSourceImpl from "../../data/remote/post_remote_source";
import PostRepository from "../../domain/adapters/repositories/post_repository";
import Post from "../../domain/entities/post";
import { RegisteredUser } from "../../domain/entities/user";
import NetworkAdapter from "../network_adapter";

export abstract class PostRemoteSource {
    abstract getPosts(user?: RegisteredUser): Post[];
    abstract getPost(postID: number): Post;
}

export abstract class PostLocalSource {
    abstract getPosts(): Post[];
    abstract getPost(postID: number): Post;
}

@injectable()
export default class PostRepositoryImpl implements PostRepository {
    @inject(PostRemoteSource) private postRemoteSource!: PostRemoteSource;
    @inject(PostLocalSource) private postLocalSource!: PostLocalSource;
    @inject(NetworkAdapter) private networkAdapter!: NetworkAdapter;

    getPost(postID: number): Post {
        if (this.networkAdapter.isOffline()) return this.postLocalSource.getPost(postID);

        return this.postRemoteSource.getPost(postID);
    }

    getPosts(user?: RegisteredUser): Post[] {
        if (this.networkAdapter.isOffline()) return this.postLocalSource.getPosts();

        return this.postRemoteSource.getPosts(user);
    }

    searchPost(searchTerm: string): Post[] {
        throw new Error("Method not implemented.");
    }
}