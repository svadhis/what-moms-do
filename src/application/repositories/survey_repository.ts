import { inject, injectable } from "inversify";
import SurveyRepository from "../../domain/adapters/repositories/survey_repository";
import UserRepository from "../../domain/adapters/repositories/user_repository";
import Survey from "../../domain/entities/survey";
import survey from "../../domain/entities/survey";
import { RegisteredUser } from "../../domain/entities/user";
import NetworkAdapter from "../network_adapter";

export abstract class SurveyRemoteSource {
    abstract update(survey: Survey): Survey;
}

@injectable()
export default class SurveyRepositoryImpl implements SurveyRepository {
    @inject(SurveyRemoteSource) private surveyRemoteSource!: SurveyRemoteSource;
    @inject(NetworkAdapter) private networkAdapter!: NetworkAdapter;

    update(survey: survey): Survey {
        if (this.networkAdapter.isOffline()) return survey;

        return this.surveyRemoteSource.update(survey);
    }
    searchSurvey(searchTerm: string): survey[] {
        throw new Error("Method not implemented.");
    }
    getSurveys(user: RegisteredUser): survey[] {
        throw new Error("Method not implemented.");
    }
}