import { inject, injectable } from "inversify";
import UserRepository from "../../domain/adapters/repositories/user_repository";
import { RegisteredUser } from "../../domain/entities/user";
import NetworkAdapter from "../network_adapter";

export abstract class UserRemoteSource {
    abstract update(user: RegisteredUser): RegisteredUser;
}

@injectable()
export default class UserRepositoryImpl implements UserRepository {
    @inject(UserRemoteSource) private userRemoteSource!: UserRemoteSource;
    @inject(NetworkAdapter) private networkAdapter!: NetworkAdapter;

    update(user: RegisteredUser): RegisteredUser {
        if (this.networkAdapter.isOffline()) return user;

        return this.userRemoteSource.update(user);
    }

    signUp(email: string): RegisteredUser {
        throw new Error("Method not implemented.");
    }

    signIn(email: string): RegisteredUser {
        throw new Error("Method not implemented.");
    }

    signOut(user: RegisteredUser): void {
        throw new Error("Method not implemented.");
    }
}