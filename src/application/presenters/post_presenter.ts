import { injectable } from "inversify";
import navigateTo from "../../core/utils/navigate_to";
import Route from "../../core/utils/route";
import PostPresenter from "../../domain/adapters/presenters/post_presenter";
import Post from "../../domain/entities/post";
import { postsState, postState } from "../../view/states/blog_states";
import PostViewModel from "../viewmodels/post_viewmodel";

@injectable()
export default class PostPresenterImpl implements PostPresenter {
    displayPost(post: Post): void {
        postState.set(_ => new PostViewModel(post));
    }

    displayPosts(posts: Post[]): void {
        postsState.set(_ => posts.map(p => new PostViewModel(p)));
        // navigateTo(Route.BLOG);
    }

    displaySearchResult(posts: Post[]): void {
        throw new Error("Method not implemented.");
    }
}
