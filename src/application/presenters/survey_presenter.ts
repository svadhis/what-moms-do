import { injectable } from "inversify";
import SurveyPresenter from "../../domain/adapters/presenters/survey_presenter";
import survey from "../../domain/entities/survey";
import { surveyState } from "../../view/states/survey_states";

@injectable()
export default class SurveyPresenterImpl implements SurveyPresenter {
    displaySurveyStats(survey: survey): void {
        surveyState.set(_ => survey);
    }

    displaySearchResult(surveys: survey[]): void {
        throw new Error("Method not implemented.");
    }
}
